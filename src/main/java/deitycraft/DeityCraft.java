package deitycraft;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import deitycraft.blocks.ModBlocks;
import deitycraft.items.ModItems;
import deitycraft.lib.Constants;
import deitycraft.lib.CreativeTab;
import deitycraft.lib.Materials;
import deitycraft.lib.Recipes;
import deitycraft.lib.TileEntities;
import deitycraft.proxy.CommonProxy;

@Mod(modid = Constants.MODID, name = Constants.MODNAME, version = Constants.VERSION)
public class DeityCraft {
	
	@Mod.Instance(Constants.MODID)
	public static DeityCraft instance;
	
	@SidedProxy(clientSide="deitycraft.proxy.ClientProxy", serverSide="deitycraft.proxy.CommonProxy")
	public static CommonProxy proxy;
	
    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
   
    	CreativeTab.init();
    	ModItems.init();
    	ModBlocks.init();
    }
 
    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
    	
    	Materials.EnumMaterials();
    	Recipes.AddRecipes();
    	proxy.registerRenderers();
    	TileEntities.RegisterTileEntities();
    }
    
    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event) {
 
    }

}
