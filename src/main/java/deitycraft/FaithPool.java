package deitycraft;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagInt;
import net.minecraft.util.ChatComponentText;
import deitycraft.gods.Deity;

/**
 * Generic class for interacting with faith pools.
 *
 */
public class FaithPool {
	
	/**
	 * Adds to a player's faith pool for a specific deity
	 * @param player
	 * @param deity
	 * @param faith
	 */
	public static void addFaith(EntityPlayer player, Deity deity, int faith) {
		NBTTagCompound tag = player.getEntityData();
		String faithTag = "Faith_" + deity.getId();
		
        NBTBase modeTag = tag.getTag(faithTag);
        if(modeTag == null) {
        	modeTag = new NBTTagInt(0);
        }
		tag.setInteger(faithTag, ((NBTTagInt)modeTag).func_150287_d() + faith);
    	//TODO: Decide exactly when we want to display available faith
		modeTag = tag.getTag(faithTag);
		player.addChatMessage(new ChatComponentText("Current Faith: " + ((NBTTagInt)modeTag).func_150287_d()));
	}
	
	/**
	 * Spends faith from a player's faith pool for a specific deity. Returns true if successful, false if the player did not have enough
	 * faith.
	 * @param player
	 * @param deity
	 * @param faith
	 * @return
	 */
	public static boolean spendFaith(EntityPlayer player, Deity deity, int faith) {
		NBTTagCompound tag = player.getEntityData();
		String faithTag = "Faith_" + deity.getId();
		
        NBTBase modeTag = tag.getTag(faithTag);
        if(modeTag == null) {
        	modeTag = new NBTTagInt(0);
        }
        if(faith > ((NBTTagInt)modeTag).func_150287_d()) {
        	return false;
        } else {
        	tag.setInteger(faithTag, ((NBTTagInt)modeTag).func_150287_d() - faith);
        	return true;
        }
	}
	
	
	/**
	 * Check the amount of faith in a player's pool for a specific deity.
	 * @param player
	 * @param deity
	 * @return
	 */
	public static int getFaith(EntityPlayer player, Deity deity) {
		NBTTagCompound tag = player.getEntityData();
		String faithTag = "Faith_" + deity.getId();
		NBTBase modeTag = tag.getTag(faithTag);
		if(modeTag == null) {
        	modeTag = new NBTTagInt(0);
        }
        
        return ((NBTTagInt)modeTag).func_150287_d();
	}
	
	
	public static void echoInsufficientFaith(EntityPlayer player, Deity deity) {
		player.addChatMessage(new ChatComponentText("Your faith in " + deity.getName() + " is lacking."));
	}

}
