package deitycraft.blocks;

import cpw.mods.fml.common.registry.GameRegistry;
import deitycraft.lib.Constants;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;

public class SunJar extends BlockContainer {
	// TODO: Jar will hold some stacks of a single kind of item like a barrel.
	// Possibly the items inside could have some in-mod significance.
	// Doubles as a test item for various item APIs and methods.
	
	private String name = "sunJar";
	
	public SunJar(){
		super(Material.rock); // Set material to rock.
		this.setCreativeTab(CreativeTabs.tabBlock); // Add block to creative tab 'Block'.
		this.setBlockName(Constants.MODID + "_" + name); // Sets internal block name.
		setBlockTextureName(Constants.MODID + ":" + name + "Icon"); // Specifies where texture in resource pack is located.
		GameRegistry.registerBlock(this, name); // Registers the block in Forge.
	}
	
	@Override
    public int getRenderType() {
    	return -1; // Disable normal rendering.
	}
    
	@Override
    public boolean isOpaqueCube()
    {
        return false; // We set this false for pretty much anything that isn't a standard block.
    }
    
    public boolean renderAsNormalBlock()
    {
        return false; // Sort of ties in to the above.
    }

	@Override
	public TileEntity createNewTileEntity(World world, int metadata) {
		return new SunJarEntity();
	}
	
}
