package deitycraft.blocks;

import cpw.mods.fml.common.registry.GameRegistry;
import deitycraft.lib.Constants;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.util.IIcon;

public class SunJarRaw extends Block {
	// TODO: This is a non-functional form of the jar.
	// Implement sun curing as with blockSandyClay.
	
	private String name = "sunJarRaw";
	
	private IIcon[] icons = new IIcon[6];
	
	
	public SunJarRaw(){
		super(Material.clay); // Set material to clay.
		this.setCreativeTab(CreativeTabs.tabBlock); // Add block to creative tab 'Block'.
		this.setBlockName(Constants.MODID + "_" + name); // Sets internal block name.
		setBlockTextureName(Constants.MODID + ":" + name); // Specifies where texture in resource pack is located.
		GameRegistry.registerBlock(this, name); // Registers the block in Forge.
	}
	
	@Override
	public void registerBlockIcons(IIconRegister iconRegister) {
		for (int i = 0; i < icons.length; i++) { // Get icons for all sides 0 - 5.
			icons[i] = iconRegister.registerIcon(Constants.MODID + ":" + name + i); // Register each side's graphics in the icon array.
		}
	}

	@Override
	public IIcon getIcon(int side, int meta) {
	    return icons[side]; // Return the array of icons that comprise our block.  
	}
	
    public boolean isOpaqueCube()
    {
        return false; // We set this false for pretty much anything that isn't a standard block.
    }
    
    public boolean renderAsNormalBlock()
    {
        return false; // Sort of ties in to the above.
    }
}
