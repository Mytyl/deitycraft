package deitycraft.blocks;

import cpw.mods.fml.common.registry.GameRegistry;
import deitycraft.lib.Constants;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.util.IIcon;

public class BlockCoronium extends Block {
	// TODO: Set this up as a light source.
	private String name = "blockCoronium";
	
	public BlockCoronium(){
		super(Material.iron); // Set material to iron.
		this.setCreativeTab(CreativeTabs.tabBlock); // Add block to creative tab 'Block'.
		this.setBlockName(Constants.MODID + "_" + name); // Sets internal block name.
		setBlockTextureName(Constants.MODID + ":" + name); // Specifies where texture in resource pack is located.
		GameRegistry.registerBlock(this, name); // Registers the block in Forge.
	}
}
