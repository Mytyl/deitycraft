package deitycraft.blocks;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;

public class ModBlocks {

	public static Block sunAltar;
	public static Block sunJug;
	public static Block sunJar;
	public static Block sunJugRaw;
	public static Block sunJarRaw;
	public static Block blockSandyClay;
	public static Block blockSandyClayHard;
	public static Block blockSandyClayBrick;
	public static Block blockSandyClayLapis;
	public static Block blockSandyClayGold;
	public static Block blockSandyClayRed;
	public static Block blockCoronium;
	
	public static void init(){
	
		sunAltar			= new SunAltar();
		sunJug				= new SunJug();
		sunJar				= new SunJar();
		sunJugRaw			= new SunJugRaw();
		sunJarRaw			= new SunJarRaw();
		blockSandyClay		= new BlockSandyClay();
		blockSandyClayHard	= new BlockSandyClayHard();
		blockSandyClayBrick	= new BlockSandyClayBrick();
		blockSandyClayLapis	= new BlockSandyClayLapis();
		blockSandyClayGold	= new BlockSandyClayGold();
		blockSandyClayRed	= new BlockSandyClayRed();
		blockCoronium		= new BlockCoronium();
	}
}
