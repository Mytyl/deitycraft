package deitycraft.lib;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import cpw.mods.fml.common.registry.GameRegistry;
import deitycraft.items.ModItems;
import deitycraft.blocks.ModBlocks;

public class Recipes {
	// Recipe management junk goes in here.
	
	public static void AddRecipes() {
		// 2 sand and 2 clay balls to 4 sandy clay.
		GameRegistry.addRecipe(new ItemStack(ModItems.sandyClay, 4), "xy", "yx", 'x', Blocks.sand, 'y', Items.clay_ball);
		GameRegistry.addRecipe(new ItemStack(ModItems.sandyClay, 4), "yx", "xy", 'x', Blocks.sand, 'y', Items.clay_ball);
		
		// 4 sandy clay to 1 sandy clay block.
		GameRegistry.addRecipe(new ItemStack(ModBlocks.blockSandyClay), "xx", "xx", 'x', ModItems.sandyClay);
		
		// 4 hardened sandy clay to 4 sandy clay bricks.
		GameRegistry.addRecipe(new ItemStack(ModBlocks.blockSandyClayBrick, 4), "xx", "xx", 'x', ModBlocks.blockSandyClayHard);
		
		// 8 clay bricks around 1 lapis, redstone or gold nugget to 8 inlaid bricks.
		GameRegistry.addRecipe(new ItemStack(ModBlocks.blockSandyClayLapis, 8), "xxx", "xyx", "xxx", 'x', ModBlocks.blockSandyClayBrick, 'y', new ItemStack(Items.dye, 1, 4));
		GameRegistry.addRecipe(new ItemStack(ModBlocks.blockSandyClayGold, 8), "xxx", "xyx", "xxx", 'x', ModBlocks.blockSandyClayBrick, 'y', Items.gold_nugget);
		GameRegistry.addRecipe(new ItemStack(ModBlocks.blockSandyClayRed, 8), "xxx", "xyx", "xxx", 'x', ModBlocks.blockSandyClayBrick, 'y', Items.redstone);
		
		// 9 coronium ingots to 1 coronium block and vice versa.
		GameRegistry.addRecipe(new ItemStack(ModBlocks.blockCoronium), "xxx", "xxx", "xxx", 'x', ModItems.ingotCoronium);
		GameRegistry.addRecipe(new ItemStack(ModItems.ingotCoronium, 9), "x", 'x', ModBlocks.blockCoronium);
		
		// 2 coronium ingots and 1 stick to a coronium sword.
		GameRegistry.addRecipe(new ItemStack(ModItems.swordCoronium), "x", "x", "y", 'x', ModItems.ingotCoronium, 'y', Items.stick);
		
		// Solarium.
		GameRegistry.addRecipe(new ItemStack(ModBlocks.sunAltar), "aba", "bcb", "ddd",
		'a', Items.gold_nugget,
		'b', Blocks.glass_pane,
		'c', Items.diamond,
		'd', ModBlocks.blockSandyClayGold);
		
		// Unfired clay jar.
		GameRegistry.addRecipe(new ItemStack(ModBlocks.sunJarRaw), "x x", "xyx", "xxx", 'x', ModItems.sandyClay, 'y', Blocks.chest);
		
		// Unfired clay jug.
		GameRegistry.addRecipe(new ItemStack(ModBlocks.sunJugRaw), "x x", "xyx", "xxx", 'x', ModItems.sandyClay, 'y', Blocks.cauldron);
	}

}
