package deitycraft.lib;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.Item;

public class CreativeTab {
	// TODO: Actually cram this mod's blocks in this tab.

	public static void init() {
		CreativeTabs tabCustom = new CreativeTabs("tabDeityCraft") {
		    @Override
		    @SideOnly(Side.CLIENT)
		    public Item getTabIconItem() {
		        return Items.nether_star;
		    }
		};
	}

}
