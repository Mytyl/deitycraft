package deitycraft.lib;

import cpw.mods.fml.common.registry.GameRegistry;

public class TileEntities {
	public static void RegisterTileEntities() {
		GameRegistry.registerTileEntity(deitycraft.blocks.SunAltarEntity.class,  "tileEntitySunAltar");
		GameRegistry.registerTileEntity(deitycraft.blocks.SunJarEntity.class, "tileEntitySunJar");
		GameRegistry.registerTileEntity(deitycraft.blocks.SunJugEntity.class, "tileEntitySunJug");
	}
}
