package deitycraft.proxy;

import cpw.mods.fml.client.registry.ClientRegistry;
import deitycraft.render.SunAltarEntityRenderer;
import deitycraft.render.SunJarEntityRenderer;
import deitycraft.render.SunJugEntityRenderer;

public class ClientProxy extends CommonProxy {

	@Override
	public void registerRenderers() {
		ClientRegistry.bindTileEntitySpecialRenderer(deitycraft.blocks.SunAltarEntity.class, new SunAltarEntityRenderer());		
		ClientRegistry.bindTileEntitySpecialRenderer(deitycraft.blocks.SunJarEntity.class, new SunJarEntityRenderer());
		ClientRegistry.bindTileEntitySpecialRenderer(deitycraft.blocks.SunJugEntity.class, new SunJugEntityRenderer());
	}
}
