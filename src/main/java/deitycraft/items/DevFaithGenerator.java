package deitycraft.items;

import cpw.mods.fml.common.registry.GameRegistry;
import deitycraft.FaithPool;
import deitycraft.gods.Deity;
import deitycraft.gods.SunGod;
import deitycraft.lib.Constants;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class DevFaithGenerator extends Item {
	
private String name = "devFaithGenerator";
private Deity deity;
    
    public DevFaithGenerator() {
        
        setUnlocalizedName(Constants.MODID + "_" + name);
        GameRegistry.registerItem(this, name);
        setCreativeTab(CreativeTabs.tabMaterials);
        setTextureName(Constants.MODID + ":" + name);
        //Just a test tool, we've only got the one god for now.
        this.deity = new SunGod();
    }
    
    public ItemStack onItemRightClick(ItemStack itemStack, World world, EntityPlayer player)
    {
        if(!world.isRemote) {
	    	if (player.capabilities.isCreativeMode)
	        {
	            FaithPool.addFaith(player, deity, 100);
	            world.playSoundAtEntity(player, "random.levelup", 0.5F, 1.0F);
	        }
        }
        return itemStack;
    }

}
