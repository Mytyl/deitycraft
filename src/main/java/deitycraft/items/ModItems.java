package deitycraft.items;

import cpw.mods.fml.common.registry.GameRegistry;
import deitycraft.items.miracles.Illumination;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;


public class ModItems {
	public static Item sandyClay;
	public static Item ingotCoronium;
	public static Item swordCoronium;
	
	//Miracles
	public static Item miracleIllumination;
	
	//Debug item for generating faith
	public static Item devFaithGenerator;
	
	public static void init() {
		sandyClay = new SandyClay();
		ingotCoronium = new IngotCoronium();
		swordCoronium = new SwordCoronium();
		
		//Miracles
		miracleIllumination = new Illumination();
		
		//Debug
		devFaithGenerator = new DevFaithGenerator();
	}
	
}
