package deitycraft.items;

import cpw.mods.fml.common.registry.GameRegistry;
import deitycraft.lib.Constants;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class SandyClay extends Item {
	// TODO: Some way to use it as vanilla clay in recipes in an emergency, but not vice versa.
	
	private String name = "sandyClay";
    
    public SandyClay() {
        
        setUnlocalizedName(Constants.MODID + "_" + name);
        GameRegistry.registerItem(this, name);
        setCreativeTab(CreativeTabs.tabMaterials);
        setTextureName(Constants.MODID + ":" + name);
    }
}
