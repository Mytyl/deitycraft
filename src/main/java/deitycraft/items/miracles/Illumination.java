package deitycraft.items.miracles;

import static net.minecraftforge.common.util.ForgeDirection.EAST;
import static net.minecraftforge.common.util.ForgeDirection.NORTH;
import static net.minecraftforge.common.util.ForgeDirection.SOUTH;
import static net.minecraftforge.common.util.ForgeDirection.WEST;
import cpw.mods.fml.common.registry.GameRegistry;
import deitycraft.FaithPool;
import deitycraft.gods.Deity;
import deitycraft.gods.SunGod;
import deitycraft.lib.Constants;
import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

/**
 * Miracle which creates a light source in the world
 *
 */
//TODO: Torch placement is buggy
//TODO: Replace torches with custom light source of some sort
//TODO: Figure out how to support multiple deities?
public class Illumination extends Item implements Miracle {

private static String name = "illuminationMiracle";

private Deity deity = new SunGod();

private static int faithCost = 10;
    
    public Illumination() {
        
        setUnlocalizedName(Constants.MODID + "_" + name);
        GameRegistry.registerItem(this, name);
        setCreativeTab(CreativeTabs.tabMaterials);
        setTextureName(Constants.MODID + ":" + name);
    }
    
    
    public boolean onItemUse(ItemStack itemStack, EntityPlayer player, World world, int x, int y, int z, int side, float p_77648_8_, float p_77648_9_, float p_77648_10_) {
    	 if(!world.isRemote) {
	    	if(canPlaceTorch(world, x, y, z)) {
	    		switch(side) {
	    		case 0:
	    			return false;
	    		case 1:
	    			//Top
	    			y++;
	    			break;
	    		case 2:
	    			//North
	    			z--;
	    			break;
	    		case 3:
	    			//South
	    			z++;
	    			break;
	    		case 4:
	    			//West
	    			x--;
	    			break;
	    		case 5:
	    			//East
	    			x++;
	    			break;
	    		}
	    		if(FaithPool.spendFaith(player, deity, faithCost)) {
	    			world.setBlock(x, y, z, Blocks.torch);
	    		} else {
	    			FaithPool.echoInsufficientFaith(player, deity);
	    		}
	    		return true;
	    	} else {
	    		return false;
	    	}
    	 } else {
    		 return false;
    	 }
    }
    
    /**
     * Copying the logic used for the placing of vanilla torches
     * @param world
     * @param x
     * @param y
     * @param z
     * @return
     */
    private boolean canPlaceTorch(World world, int x, int y, int z) {
    	boolean canPlaceOnTop = false;
    	 if (World.doesBlockHaveSolidTopSurface(world, x, y, z))
         {
             canPlaceOnTop = true;
         }
         else
         {
             Block block = world.getBlock(x, y, z);
             canPlaceOnTop = block.canPlaceTorchOnTop(world, x, y, z);
         }
    	
    	return world.isSideSolid(x - 1, y, z, EAST,  true) ||
                world.isSideSolid(x + 1, y, z, WEST,  true) ||
                world.isSideSolid(x, y, z - 1, SOUTH, true) ||
                world.isSideSolid(x, y, z + 1, NORTH, true) ||
                canPlaceOnTop;
    }
}
