package deitycraft.items;

import cpw.mods.fml.common.registry.GameRegistry;
import deitycraft.lib.Constants;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class IngotCoronium extends Item {
    private String name = "ingotCoronium";
    
    public IngotCoronium() {
        // TODO: Figure out how to specify it as an ingot in Forge.
    	// Figure out the tool material enumerator so that it can be used for more tools and eventually armor.
    	// Also coronium nuggets.
    	
        setUnlocalizedName(Constants.MODID + "_" + name);
        GameRegistry.registerItem(this, name);
        setCreativeTab(CreativeTabs.tabMaterials);
        setTextureName(Constants.MODID + ":" + name);
    }
}
