package deitycraft.items;

import cpw.mods.fml.common.registry.GameRegistry;
import deitycraft.lib.Constants;
import deitycraft.lib.Materials;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.ItemSword;

public class SwordCoronium extends ItemSword {
	// Temporarily using gold as the material until I can figure out the material enumerator.
	// TODO: Tool material enumerator!!
	// Implement Smite VI effect, probably part of recipes would be easiest.
	// Perhaps setting undead aflame on hit would be a fun test and thematic.
	// It needs some kind of flaming sound effect when this happens!
	
	private String name = "swordCoronium";
    
    public SwordCoronium() {
        super(ToolMaterial.GOLD);
    	setUnlocalizedName(Constants.MODID + "_" + name);
        GameRegistry.registerItem(this, name);
        setCreativeTab(CreativeTabs.tabMaterials);
        setTextureName(Constants.MODID + ":" + name);
    }
}
