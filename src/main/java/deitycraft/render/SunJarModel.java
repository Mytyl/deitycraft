// Date: 9/17/2014 12:39:18 AM
// Template version 1.1
// Java generated by Techne
// Keep in mind that you still need to fill in some blanks
// - ZeuX

package deitycraft.render;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class SunJarModel extends ModelBase
{
  //fields
    ModelRenderer Base;
    ModelRenderer Side1;
    ModelRenderer Side2;
    ModelRenderer Side3;
    ModelRenderer Side4;
    ModelRenderer Upper;
    ModelRenderer Upper2;
    ModelRenderer Neck;
    ModelRenderer Lip;
  
  public SunJarModel()
  {
    textureWidth = 64;
    textureHeight = 64;
    
      Base = new ModelRenderer(this, 1, 50);
      Base.addBox(0F, 0F, 0F, 12, 1, 12);
      Base.setRotationPoint(-6F, 23F, -6F);
      Base.setTextureSize(64, 64);
      Base.mirror = true;
      setRotation(Base, 0F, 0F, 0F);
      Side1 = new ModelRenderer(this, 1, 43);
      Side1.addBox(0F, 0F, 0F, 12, 1, 5);
      Side1.setRotationPoint(-6F, 23F, -7F);
      Side1.setTextureSize(64, 64);
      Side1.mirror = true;
      setRotation(Side1, 1.570796F, 0F, 0F);
      Side2 = new ModelRenderer(this, 1, 43);
      Side2.addBox(0F, 0F, 0F, 12, 1, 5);
      Side2.setRotationPoint(-7F, 23F, 6F);
      Side2.setTextureSize(64, 64);
      Side2.mirror = true;
      setRotation(Side2, 1.570796F, 1.570796F, 0F);
      Side3 = new ModelRenderer(this, 1, 43);
      Side3.addBox(0F, 0F, 0F, 12, 1, 5);
      Side3.setRotationPoint(6F, 23F, 7F);
      Side3.setTextureSize(64, 64);
      Side3.mirror = true;
      setRotation(Side3, 1.570796F, 3.141593F, 0F);
      Side4 = new ModelRenderer(this, 1, 43);
      Side4.addBox(0F, 0F, 0F, 12, 1, 5);
      Side4.setRotationPoint(7F, 23F, -6F);
      Side4.setTextureSize(64, 64);
      Side4.mirror = true;
      setRotation(Side4, 1.570796F, 4.712389F, 0F);
      Upper = new ModelRenderer(this, 1, 50);
      Upper.addBox(0F, 0F, 0F, 12, 1, 12);
      Upper.setRotationPoint(-6F, 17F, -6F);
      Upper.setTextureSize(64, 64);
      Upper.mirror = true;
      setRotation(Upper, 0F, 0F, 0F);
      Upper2 = new ModelRenderer(this, 1, 31);
      Upper2.addBox(0F, 0F, 0F, 10, 1, 10);
      Upper2.setRotationPoint(-5F, 16F, -5F);
      Upper2.setTextureSize(64, 64);
      Upper2.mirror = true;
      setRotation(Upper2, 0F, 0F, 0F);
      Neck = new ModelRenderer(this, 1, 21);
      Neck.addBox(0F, 0F, 0F, 6, 3, 6);
      Neck.setRotationPoint(-3F, 13F, -3F);
      Neck.setTextureSize(64, 64);
      Neck.mirror = true;
      setRotation(Neck, 0F, 0F, 0F);
      Lip = new ModelRenderer(this, 1, 11);
      Lip.addBox(0F, 0F, 0F, 8, 1, 8);
      Lip.setRotationPoint(-4F, 12F, -4F);
      Lip.setTextureSize(64, 64);
      Lip.mirror = true;
      setRotation(Lip, 0F, 0F, 0F);
  }
  
  public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
  {
    super.render(entity, f, f1, f2, f3, f4, f5);
    setRotationAngles(f, f1, f2, f3, f4, f5, entity);
    Base.render(f5);
    Side1.render(f5);
    Side2.render(f5);
    Side3.render(f5);
    Side4.render(f5);
    Upper.render(f5);
    Upper2.render(f5);
    Neck.render(f5);
    Lip.render(f5);
  }
  
  private void setRotation(ModelRenderer model, float x, float y, float z)
  {
    model.rotateAngleX = x;
    model.rotateAngleY = y;
    model.rotateAngleZ = z;
  }
  
  public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity)
  {
    super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
  }

}
