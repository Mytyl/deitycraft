package deitycraft.gods;

/**
 * Our first deity.
 *
 */
public class SunGod implements Deity {

	public static String name = "SunGod";
	public static int deityId = 1;
	
	@Override
	public String getName() {
		return name;
	}
	
	public int getId() {
		return deityId;
	}

}
