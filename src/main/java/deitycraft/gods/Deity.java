package deitycraft.gods;

/**
 * Interface to define the methods applicable to a god. Allows us to generically refer to gods, without necessarily worrying about which
 * one is really being referenced at the moment.
 *
 */
public interface Deity {

	public String getName();
	
	public int getId();
	
}
